import model.Circle;
import model.Rectangle;
import model.Shape;
import model.Square;

public class App {
    public static void main(String[] args) throws Exception {
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green",false);
        System.out.println(shape1);
        System.out.println(shape2);

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0,"green",true);
        System.out.println(circle1);
        System.out.println("diện tích= "+ circle1.getArea() + ", chu vi= "+ circle1.getPerimeter());
        System.out.println(circle2);
        System.out.println("diện tích= "+ circle2.getArea() + ", chu vi= "+ circle2.getPerimeter());
        System.out.println(circle3);
        System.out.println("diện tích= "+ circle3.getArea() + ", chu vi= "+ circle3.getPerimeter());


        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.5,3.5);
        Rectangle rectangle3 = new Rectangle(2.0,1.5, "green", true);
        System.out.println(rectangle1);
        System.out.println("diện tích= "+ rectangle1.getArea() + ", chu vi= "+ rectangle1.getPerimeter());
        System.out.println(rectangle2);
        System.out.println("diện tích= "+ rectangle2.getArea() + ", chu vi= "+ rectangle2.getPerimeter());
        System.out.println(rectangle3);
        System.out.println("diện tích= "+ rectangle3.getArea() + ", chu vi= "+ rectangle3.getPerimeter());


        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square(2.0, "green", true);
        System.out.println(square1);
        System.out.println("diện tích= "+ square1.getArea() + ", chu vi= "+ square1.getPerimeter());
        System.out.println(square2);
        System.out.println("diện tích= "+ square2.getArea() + ", chu vi= "+ square2.getPerimeter());
        System.out.println(square3);
        System.out.println("diện tích= "+ rectangle3.getArea() + ", chu vi= "+ square3.getPerimeter());


    }
}
